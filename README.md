# Task Manager

Features
1. List the tasks
2. Create a new task
3. Update a Task
4. Delete a Task Frontend

## Installation

1. Clone the repository: `git clone gitlab.com/public-reactjs-app/task-manager.git`
2. Install dependencies: `npm install`

## Usage

1. Start the development server: `npm start`
2. Open your browser and go to `http://localhost:3000`

## Contributing

1. Fork the repository.
2. Create a new branch: `git checkout -b feature-name`
3. Make your changes and commit them: `git commit -am 'Add new feature'`
4. Push to the branch: `git push origin feature-name`
5. Submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.