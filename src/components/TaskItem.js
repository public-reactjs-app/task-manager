import React, { useState } from 'react';
import { Button, TextField } from '@mui/material';
import styles from '../styles/TaskItem.module.scss';

const TaskItem = ({ task, updateTask, deleteTask }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newTask, setNewTask] = useState(task.text);

  const handleUpdate = () => {
    updateTask(task.id, newTask);
    setIsEditing(false);
  };

  return (
    <div className={styles.taskItem}>
      {isEditing ? (
        <>
          <TextField
            variant="outlined"
            value={newTask}
            onChange={(e) => setNewTask(e.target.value)}
            fullWidth
          />
          <Button
            variant="contained"
            color="primary"
            onClick={handleUpdate}
            style={{ marginLeft: '10px' }}
          >
            Update
          </Button>
        </>
      ) : (
        <>
          <span>{task.text}</span>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => setIsEditing(true)}
            style={{ marginLeft: '10px' }}
          >
            Edit
          </Button>
          <Button
            variant="contained"
            color="error"
            onClick={() => deleteTask(task.id)}
            style={{ marginLeft: '10px' }}
          >
            Delete
          </Button>
        </>
      )}
    </div>
  );
};

export default TaskItem;