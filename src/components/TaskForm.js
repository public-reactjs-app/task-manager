import React, { useState } from 'react';
import { Button, TextField } from '@mui/material';
import styles from '../styles/TaskForm.module.scss';

const TaskForm = ({ addTask }) => {
  const [task, setTask] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    if (task.trim()) {
      addTask(task);
      setTask('');
    }
  };

  return (
    <form className={styles.taskForm} onSubmit={handleSubmit}>
      <TextField
        label="Add a new task"
        variant="outlined"
        value={task}
        onChange={(e) => setTask(e.target.value)}
        className={styles['taskForm-input']}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={styles['taskForm-button']}
      >
        Add Task
      </Button>
    </form>
  );
};

export default TaskForm;